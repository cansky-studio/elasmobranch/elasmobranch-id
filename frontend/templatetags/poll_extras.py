from django import template
from urllib.parse import urlparse, parse_qs, urlencode

register = template.Library()

@register.filter
def get_username(value):
    return value.split("@")[0]

@register.filter
def unique_query_page(value):
    parsed_url = urlparse(value)
    query_params = parse_qs(parsed_url.query, keep_blank_values=True)
    query_params.pop('page', None)
    new_query_params = urlencode(query_params, doseq=True)
    return f"{parsed_url.path}?{new_query_params}"