from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.db.models import F, Q, Case, Value, When
from django.http import Http404
from django.shortcuts import render, redirect
from urllib.parse import unquote

from backend.models import (
    Observation,
    ObservationImage,
    ObservationReply,
    User,
    UserConnection,
    Topic,
    Thread,
    ThreadReply,
    ThreadVote,
    Article,
    Page,
    Section,
    SectionImage,
    Navigation,
    SubNavigation,
)
from frontend import utils

def static(request, slug):
    lang = utils.get_session_lang(request)    
    template_name = f'{lang}/pages/static/base.html'
    
    pages = Page.objects.filter(slug=slug)

    if not pages.exists():
        raise Http404()
    
    page = pages.first()
    preview = request.GET.get('preview', False)
    
    if not preview and not page.visibility:
        raise Http404()

    mapper = list()
    sections = Section.objects.filter(level=pages.first()).order_by('order')
    for section in sections:
        section_images = SectionImage.objects.filter(level=section).order_by('pk')
        mapper.append((section, section_images))

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'mapper': mapper,
    }

    return render(request, template_name, response)

def home(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/home.html'

    observations = Observation.objects.filter(status='VERIFIED')
    encounter_count = observations.count()
    province_count = observations.values_list('province', flat=True).distinct().count()
    species_count = observations.values_list('scientific_name', flat=True).distinct().count()

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'page': 1,
        'encounter_count': encounter_count,
        'province_count': province_count,
        'species_count': species_count,
    }
    return render(request, template_name, response)


def about(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/about.html'
    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'page': 2
    }
    return render(request, template_name, response)


def team(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/team.html'
    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'page': 2
    }
    return render(request, template_name, response)


def iecsn(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/iecsn.html'
    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'page': 2
    }
    return render(request, template_name, response)


def guidelines(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/guidelines.html'
    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'page': 3
    }
    return render(request, template_name, response)


def elasmoid(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/elasmoid.html'
    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'page': 3
    }
    return render(request, template_name, response)


def report(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/report.html'
    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'page': 4
    }
    return render(request, template_name, response)


def report_tna(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/report-tna.html'
    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'page': 4
    }
    return render(request, template_name, response)


@login_required
def form_report(request):
    lang = utils.get_session_lang(request)
    user = User.objects.get(username=request.user.username)
    page = 'report.html' if user.is_verified else 'verify.html'
    template_name = f'{lang}/forms/{page}'
    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'page': 4
    }
    return render(request, template_name, response)


def maps(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/maps.html'

    gallery = ObservationImage.objects.filter(observation__status='VERIFIED').order_by('-pk')[:6]
    years = [date.year for date in Observation.objects.dates('datetime', 'year')]

    observations = Observation.objects.filter(status='VERIFIED')
    encounter_count = observations.count()
    province_count = observations.values_list('province', flat=True).distinct().count()
    species_count = observations.values_list('scientific_name', flat=True).distinct().count()

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'page': 5,
        'gallery': gallery,
        'years': years,
        'encounter_count': encounter_count,
        'province_count': province_count,
        'species_count': species_count,
    }
    return render(request, template_name, response)


def articles(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/articles.html'

    articles = Article.objects.all().order_by('-created_at')
    
    tag = request.GET.get('tag')
    if tag:
        q_objects = Q(articletag__en_tag__icontains=tag) if lang == 'en' else Q(articletag__id_tag__icontains=tag)
        articles = articles.filter(q_objects).distinct()

    paginator = Paginator(articles, 12)
    page_number = request.GET.get('page', 1)
    paginator_object = paginator.get_page(page_number)
    page_range = paginator.get_elided_page_range(number=page_number)

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'page': 6,
        'tag': tag,
        'paginator_object': paginator_object,
        'page_range': page_range,
    }

    return render(request, template_name, response)


def article(request, pk):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/article.html'
    article = Article.objects.get(pk=pk)
    article_tags = utils.get_article_tags(article, lang)
    article_url =  "{0}://{1}{2}".format(request.scheme, request.get_host(), request.path)

    q_objects = Q()
    for tag in article_tags:
        q_objects |= Q(articletag__en_tag__icontains=tag) if lang == 'en' else Q(articletag__id_tag__icontains=tag)

    related_articles = Article.objects.all().filter(q_objects).exclude(pk=pk).distinct().order_by('-created_at')[:3]
    
    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'page': 6,
        'article': article,
        'article_url': article_url,
        'related_articles': related_articles,
    }

    return render(request, template_name, response)


def identify(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/forms/identify.html'
    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
    }
    return render(request, template_name, response)


def recovery(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/forms/recovery.html'
    token = request.GET.get('token', None)
    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'token': token
    }
    return render(request, template_name, response)


def login(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/forms/login.html'
    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
    }
    return render(request, template_name, response)


def signup(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/forms/signup.html'
    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
    }
    return render(request, template_name, response)


def search(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/search.html'

    observations = Observation.objects.filter(status='VERIFIED').order_by('-created_at')
    observation_images = ObservationImage.objects.all()

    category_shark = observations.filter(category='shark').count()
    category_ray = observations.filter(category='ray').count()
    category_chimaera = observations.filter(category='chimaera').count()

    queries = request.GET.get('query', None)

    if queries:
        queries = unquote(queries)
        for query in queries.split():
            observations = observations.filter(
                Q(common_name__icontains=query) |
                Q(scientific_name__icontains=query) |
                Q(location__icontains=query) |
                Q(province__icontains=query) |
                Q(reporter__first_name__icontains=query) |
                Q(reporter__last_name__icontains=query) |
                Q(submitter__icontains=query)
            )

    category = request.GET.get('category', None)
    if category:
        observations = observations.filter(category=category)

    mapper = list()
    for observation in observations:
        observation_image = None
        images = observation_images.filter(observation=observation)
        if images.exists():
            observation_image = images.first()

        mapper.append((observation, observation_image))

    paginator = Paginator(mapper, 5)
    page_number = request.GET.get('page', 1)
    paginator_object = paginator.get_page(page_number)
    page_range = paginator.get_elided_page_range(number=page_number)

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'query': queries,
        'category': category,
        'observations': observations,
        'paginator_object': paginator_object,
        'page_range': page_range,
        'category_shark': category_shark,
        'category_ray': category_ray,
        'category_chimaera': category_chimaera,
    }

    return render(request, template_name, response)


@login_required
def contributor_profile(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/contributors/profile.html'

    user = User.objects.get(username=request.user.username)
    observations = Observation.objects.filter(reporter=user)
    
    filter_categories = request.GET.getlist('category', ['shark', 'ray', 'chimaera'])
    if filter_categories:
        observations = observations.filter(category__in=filter_categories)

    filter_status = request.GET.getlist('status', ['VERIFIED','PENDING', 'REJECTED'])
    if filter_status:
        observations = observations.filter(status__in=filter_status)

    filter_date = request.GET.get('date', 'ASC')
    observations = observations.order_by(F('modified_at').asc() if filter_date == 'ASC' else F('modified_at').desc())
    
    observations = sort_observation_by_status(observations)
    observation_images = ObservationImage.objects.filter(observation__in=observations)

    species = observations.values_list('scientific_name', flat=True).distinct().count()

    following = UserConnection.objects.filter(follower=user)
    follower = UserConnection.objects.filter(following=user)

    mapper = [(observation, observation_images.filter(observation=observation).first()) for observation in observations]

    paginator = Paginator(mapper, 6)
    page_number = request.GET.get('page', 1)
    paginator_object = paginator.get_page(page_number)
    page_range = paginator.get_elided_page_range(number=page_number)

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'user': user,
        'observations': observations,
        'species': species,
        'following': following,
        'follower': follower,
        'filter_categories': filter_categories,
        'filter_status': filter_status,
        'filter_date': filter_date,
        'paginator_object': paginator_object,
        'page_range': page_range,
    }
    return render(request, template_name, response)


@login_required
def contributor_profile_user(request, username):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/contributors/profile-user.html'

    if username == request.user.username:
        return redirect('frontend:contributor_profile')

    user = User.objects.get(username=username)
    observations = Observation.objects.filter(reporter=user, status='VERIFIED')

    filter_categories = request.GET.getlist('category', ['shark', 'ray', 'chimaera'])
    if filter_categories:
        observations = observations.filter(category__in=filter_categories)

    filter_date = request.GET.get('date', 'ASC')
    observations = observations.order_by(F('modified_at').asc() if filter_date == 'ASC' else F('modified_at').desc())
    observation_images = ObservationImage.objects.filter(observation__in=observations)

    species = observations.values_list('scientific_name', flat=True).distinct().count()

    following = UserConnection.objects.filter(follower=user)
    is_following = UserConnection.objects.filter(follower=request.user, following=user).exists()
    follower = UserConnection.objects.filter(following=user)

    mapper = [(observation, observation_images.filter(observation=observation).first()) for observation in observations]

    paginator = Paginator(mapper, 6)
    page_number = request.GET.get('page', 1)
    paginator_object = paginator.get_page(page_number)
    page_range = paginator.get_elided_page_range(number=page_number)

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'user': user,
        'observations': observations,
        'species': species,
        'following': following,
        'is_following': is_following,
        'follower': follower,
        'filter_categories': filter_categories,
        'filter_date': filter_date,
        'paginator_object': paginator_object,
        'page_range': page_range,
    }
    return render(request, template_name, response)


@login_required
def contributor_profile_edit(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/contributors/profile-edit.html'

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'user': request.user,
    }
    return render(request, template_name, response)


@login_required
def contributor_observation(request, pk):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/contributors/observation.html'

    observation = Observation.objects.get(pk=pk)
    observation_images = ObservationImage.objects.filter(observation=observation)
    observation_image = None
    if observation_images.exists():
        observation_image = observation_images.first()
    observation_replies = ObservationReply.objects.filter(observation=observation)

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'observation': observation,
        'observation_image': observation_image,
        'observation_replies': observation_replies,
    }
    return render(request, template_name, response)


@login_required
def contributor_observations(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/contributors/observations.html'

    observations = Observation.objects.exclude(status__in=['PENDING','REJECTED']).order_by('-pk')

    filter_categories = request.GET.getlist('category', ['shark', 'ray', 'chimaera'])
    if filter_categories:
        observations = observations.filter(category__in=filter_categories)

    filter_date = request.GET.get('date', 'ASC')
    observations = observations.order_by(F('modified_at').asc() if filter_date == 'ASC' else F('modified_at').desc())
    observation_images = ObservationImage.objects.filter(observation__in=observations)

    mapper = list()
    for observation in observations:
        filtered_images = observation_images.filter(observation=observation)
        observation_image = None
        if filtered_images.exists():
            observation_image = filtered_images.first()

        mapper.append((observation, observation_image))

    paginator = Paginator(mapper, 6)
    page_number = request.GET.get('page', 1)
    paginator_object = paginator.get_page(page_number)
    page_range = paginator.get_elided_page_range(number=page_number)

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'observations': observations,
        'filter_categories': filter_categories,
        'filter_date': filter_date,
        'paginator_object': paginator_object,
        'page_range': page_range,
    }

    return render(request, template_name, response)


@login_required
def contributor_leaderboard(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/contributors/leaderboard.html'

    users = User.objects.exclude(username__in=['admin'])

    mapper = list()
    for user in users:
        score = count_contribution_score(user)
        mapper.append((user, score))

    mapper = sorted(mapper, key=lambda x: x[1], reverse=True)

    paginator = Paginator(mapper, 10)
    page_number = request.GET.get('page', 1)
    paginator_object = paginator.get_page(page_number)
    page_range = paginator.get_elided_page_range(number=page_number)

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'paginator_object': paginator_object,
        'page_range': page_range,
    }

    return render(request, template_name, response)


@login_required
def verify(request):
    lang = utils.get_session_lang(request)
    user = User.objects.get(username=request.user.username)
    template_name = f'{lang}/forms/verify.html'
    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'user': user
    }
    return render(request, template_name, response)


@login_required
def form_report_edit(request, pk):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/forms/report-edit.html'

    observation = Observation.objects.get(pk=pk)

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'observation': observation,
    }
    return render(request, template_name, response)


@login_required
def forum_topics(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/forums/topics.html'

    topics = Topic.objects.all()

    trending_topics = list()
    for topic in topics:
        num_thread = topic.thread_set.all().count()
        trending_topics.append((topic, num_thread))

    trending_topics = sorted(trending_topics, key=lambda x: x[1], reverse=True)

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'topics': topics,
        'trending_topics': trending_topics[:3],
    }

    return render(request, template_name, response)

@login_required
def forum_threads(request, slug):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/forums/threads.html'

    topic = Topic.objects.get(slug=slug)
    threads = Thread.objects.filter(topic=topic)

    mapper = list()
    for thread in threads:
        num_reply = thread.threadreply_set.all().count()
        mapper.append((thread, num_reply))

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'topic': topic,
        'mapper': mapper
    }

    return render(request, template_name, response)

@login_required
def forum_thread(request, pk):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/forums/thread.html'

    user = User.objects.get(username=request.user.username)

    thread = Thread.objects.get(pk=pk)
    thread_votes = ThreadVote.objects.filter(thread=thread)
    thread_up_vote = thread_votes.filter(vote=True).count()
    thread_down_vote = thread_votes.filter(vote=False).count()

    thread_user_vote = thread_votes.filter(user=user)
    thread_user_vote = thread_user_vote.first().vote if thread_user_vote.exists() else None

    thread_replies = ThreadReply.objects.filter(thread=thread)

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'thread': thread,
        'thread_up_vote': thread_up_vote,
        'thread_down_vote': thread_down_vote,
        'thread_user_vote': thread_user_vote,
        'thread_replies': thread_replies,
    }

    return render(request, template_name, response)

@login_required
def forum_thread_create(request, slug):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/forums/thread-create.html'

    topic = Topic.objects.get(slug=slug)

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'topic': topic
    }

    return render(request, template_name, response)

@login_required
def forum_thread_edit(request, pk):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/forums/thread-edit.html'

    thread = Thread.objects.get(pk=pk)

    response = {
        'use_navbar': True,
        'navigation': get_navigation(),
        'thread': thread,
    }

    return render(request, template_name, response)

def get_navigation():
    mapper = list()
    navigations = Navigation.objects.all().order_by('order')
    
    for navigation in navigations:
        sub_navigation = SubNavigation.objects.filter(navigation=navigation).order_by('order')
        mapper.append((navigation, sub_navigation))

    return mapper

def sort_observation_by_status(observations):
    status_order = Case(
        When(status="VERIFIED", then=Value(1)),
        When(status="PENDING", then=Value(2)),
        When(status="REJECTED", then=Value(3)),
    )
    return observations.alias(status_order=status_order).order_by('status_order')

def count_contribution_score(user):
    observations = Observation.objects.filter(reporter=user)
    verified_observation_score = observations.filter(status='VERIFIED').count() * 50
    rejected_observation_score = observations.filter(status='REJECTED').count() * 5

    observation_replies = ObservationReply.objects.filter(user=user)
    verified_observation_reply_score = observation_replies.filter(is_verified=True).count() * 10

    threads = Thread.objects.filter(user=user)
    verified_thread_score = threads.filter(is_verified=True).count() * 20
    not_verified_thread_score = threads.filter(is_verified=False).count() * 5

    thread_replies = ThreadReply.objects.filter(user=user).exists()
    thread_reply_score = 2 if thread_replies else 0

    return verified_observation_score \
           + rejected_observation_score \
           + verified_observation_reply_score \
           + verified_thread_score \
           + not_verified_thread_score \
           + thread_reply_score


def handler404(request, exception):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/404.html'
    response = {
        'use_navbar': True,
    }

    return render(request, template_name, response, status=404)


def handler500(request):
    lang = utils.get_session_lang(request)
    template_name = f'{lang}/pages/500.html'
    response = {
        'use_navbar': True,
    }

    return render(request, template_name, response, status=500)
