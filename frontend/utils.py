def set_session_lang(request, lang='en'):
    request.session['lang'] = lang


def get_session_lang(request):
    default = 'en'
    if request.session.has_key('lang'):
        return request.session.get('lang')
    elif request.user.is_authenticated:
        return request.user.language
    else:
        return default


def get_article_tags(article, lang):
    tag_field = 'en_tag' if lang == 'en' else 'id_tag'
    return [getattr(at, tag_field) for at in article.articletag_set.all()]

