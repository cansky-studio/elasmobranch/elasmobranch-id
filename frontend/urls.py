from django.urls import path
from django.views import defaults
from frontend import views

app_name = 'frontend'

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('team/', views.team, name='team'),
    path('guidelines/', views.guidelines, name='guidelines'),
    path('elasmoid/', views.elasmoid, name='elasmoid'),
    path('iecsn/', views.iecsn, name='iecsn'),
    path('maps/', views.maps, name='maps'),
    path('articles/', views.articles, name='articles'),
    path('articles/<int:pk>/', views.article, name='article'),
    path('report/', views.report, name='report'),
    path('report/tna/', views.report_tna, name='report_tna'),
    path('search/', views.search, name='search'),
    path('s/<slug>/', views.static, name='static'),

    path('signup/', views.signup, name='signup'),
    path('login/', views.login, name='login'),
    path('login/identify/', views.identify, name='identify'),
    path('recovery/', views.recovery, name='recovery'),
    path('verify/', views.verify, name='verify'),

    path('contributor/observations/', views.contributor_observations, name='contributor_observations'),
    path('contributor/observation/<int:pk>/', views.contributor_observation, name='contributor_observation'),
    path('contributor/leaderboard/', views.contributor_leaderboard, name='contributor_leaderboard'),
    path('contributor/profile/', views.contributor_profile, name='contributor_profile'),
    path('contributor/profile/edit/', views.contributor_profile_edit, name='contributor_profile_edit'),
    path('contributor/profile/<username>/', views.contributor_profile_user, name='contributor_profile_user'),

    path('forum/topics/', views.forum_topics, name='forum_topics'),
    path('forum/thread/<int:pk>/', views.forum_thread, name='forum_thread'),
    path('forum/thread/<int:pk>/edit/', views.forum_thread_edit, name='forum_thread_edit'),
    path('forum/threads/<slug>/', views.forum_threads, name='forum_threads'),
    path('forum/threads/<slug>/create', views.forum_thread_create, name='forum_thread_create'),

    path('form/report', views.form_report, name='form_report'),
    path('form/report/<int:pk>/edit', views.form_report_edit, name='form_report_edit'),
]
