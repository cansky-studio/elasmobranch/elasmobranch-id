$(document).ready(function () {
    let options = {
        useEasing: true,
        useGrouping: true,
        separator: ',',
        decimal: '.',
    };

    function countStart({encounter, province, species}) {
        let encounterCount = new CountUp("encounterCount", 0, encounter, 0, 4, options);
        let provinceCount = new CountUp("provinceCount", 0, province, 0, 6, options);
        let speciesCount = new CountUp("speciesCount", 0, species, 0, 6, options);
        if (!encounterCount.error && !provinceCount.error) {
            encounterCount.start();
            provinceCount.start();
            speciesCount.start();
        }
    }

    function isScrolledIntoView(elem) {
        let docViewTop = $(window).scrollTop();
        let docViewBottom = docViewTop + $(window).height();

        let elemTop = $(elem).offset().top;
        let elemBottom = elemTop + $(elem).height();

        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

    let animateCount = false;
    $(window).on('scroll', function () {
        let elem = $("#encounterCount");
        if (!animateCount && elem.length && isScrolledIntoView(elem)) {
            let encounter = parseInt($("#encounterCount").data("encounter"));
            let province = parseInt($("#provinceCount").data("province"));
            let species = parseInt($("#speciesCount").data("species"));

            countStart({
                encounter: encounter,
                province: province,
                species: species,
            });
            animateCount = true;
        }
    });

    // secure content
    window.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    }, false);

    $('body').bind('cut copy', function (e) {
        e.preventDefault();
    });
});