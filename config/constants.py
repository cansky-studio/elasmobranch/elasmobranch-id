import enum


class Mailer(enum.Enum):
    DEWAWEB = 1
    GMAIL = 2
