"""
Django settings for config project.

Generated by 'django-admin startproject' using Django 3.2.12.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.2/ref/settings/
"""

from pathlib import Path
from config.constants import Mailer

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-f1v1-+ku*@1%8%y6@*r+h20qu2r)xlat4ytypinl2h3yur-)l@'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['www.elasmobranch.id', 'elasmobranch.id']

# Application definition

ELASMOBRANCH_APPS = [
    'frontend',
    'backend',
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'import_export',
    'ckeditor',
    'nested_admin',
]

INSTALLED_APPS += ELASMOBRANCH_APPS

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    "whitenoise.middleware.WhiteNoiseMiddleware",
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.media',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'elasmobr_prod',
        'USER': 'elasmobr_admin',
        'PASSWORD': 'Rhinaancylostoma2020!!',
        'HOST': 'localhost',
        'PORT': '5432',
    },
    'dev': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    },
    'staging': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'elasmobr_staging',
        'USER': 'elasmobr_admin',
        'PASSWORD': 'Rhinaancylostoma2020!!',
        'HOST': 'localhost',
        'PORT': '5432',
    },
    'production': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'elasmobr_prod',
        'USER': 'elasmobr_admin',
        'PASSWORD': 'Rhinaancylostoma2020!!',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}

# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = 'backend.User'

LOGIN_URL = '/login'

# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Jakarta'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

CKEDITOR_CONFIGS = {
    'default': {
        'versionCheck': False,
        'skin': 'moono',
        'toolbar': [
            ['TextColor', 'BGColor'],
            ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript'],
            ['Styles', 'Format', 'Font', 'FontSize'],
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['NumberedList', 'BulletedList', 'Outdent', 'Indent', 'Blockquote'],
            ['Link', 'Unlink', 'Anchor'],
            ['Image', 'Table', 'HorizontalRule', 'SpecialChar'],
            ['Source', 'Maximize'],
        ],
    }
}

STATIC_URL = '/static/'
STATIC_ROOT = BASE_DIR / 'static'
STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"

MEDIA_URL = '/media/'
MEDIA_ROOT = BASE_DIR / 'media'

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

DEFAULT_MAILER = Mailer.GMAIL

if DEFAULT_MAILER == Mailer.DEWAWEB:
    EMAIL_USE_SSL = True
    EMAIL_PORT = 465
    EMAIL_HOST = 'elasmobranch.canskystudio.com'
    EMAIL_HOST_USER = 'no-reply@elasmobranch.canskystudio.com'
    EMAIL_HOST_PASSWORD = 'Cansky1234'
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
elif DEFAULT_MAILER == Mailer.GMAIL:
    EMAIL_USE_TLS = True
    EMAIL_PORT = 587
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_HOST_USER = 'elasmobranch.pi@gmail.com'
    EMAIL_HOST_PASSWORD = 'nqxh yyix ctwn plry'
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# CSRF_COOKIE_SECURE = False
# SESSION_COOKIE_SECURE = False
