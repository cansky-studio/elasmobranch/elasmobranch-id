from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.html import mark_safe
from ckeditor.fields import RichTextField

class User(AbstractUser):
    LANGUAGES = [
        ('en', 'English'),
        ('id', 'Bahasa Indonesia'),
    ]

    phone = models.CharField(max_length=20, blank=True, null=True)
    job = models.CharField(max_length=255, blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    joined_at = models.DateField(auto_now_add=True, blank=True, null=True)
    language = models.CharField(choices=LANGUAGES, default='en', max_length=100)
    token = models.CharField(max_length=255, blank=True, null=True)
    is_verified = models.BooleanField(default=False)
    description = models.TextField(blank=True, null=True)
    image = models.ImageField(upload_to='profiles/', blank=True, null=True)

    def __str__(self):
        return self.full_name()

    def full_name(self):
        return f'{self.first_name} {self.last_name}'


class UserConnection(models.Model):
    follower = models.ForeignKey(User, on_delete=models.CASCADE, related_name='follower')
    following = models.ForeignKey(User, on_delete=models.CASCADE, related_name='following')
    followed_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)


class Observation(models.Model):
    CATEGORIES = (
        ('shark', 'Shark'),
        ('ray', 'Ray & Skates'),
        ('chimaera', 'Chimaera'),
    )

    TYPES = (
        ('sighting', 'Sighting'),
        ('landing', 'Landing'),
    )

    STATUS = (
        ('PENDING', 'PENDING'),
        ('REJECTED', 'REJECTED'),
        ('VERIFIED', 'VERIFIED'),
    )

    SEX = (
        ('male', 'Male'),
        ('female', 'Female'),
        ('unidentified', 'Unidentified'),
    )

    REJECT_REASON = (
        ('SU', 'Species unidentified'),
        ('DD', 'Double data'),
        ('OOD', 'Out of date'),
    )

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    datetime = models.DateTimeField(blank=True, null=True)
    category = models.CharField(choices=CATEGORIES, max_length=255, default='shark', blank=True, null=True)
    type = models.CharField(choices=TYPES, max_length=255, default='sighting', blank=True, null=True)
    location = models.CharField(max_length=255, blank=True, null=True)
    province = models.CharField(max_length=255, blank=True, null=True)
    latitude = models.DecimalField(max_digits=30, decimal_places=22, blank=True, null=True)
    longitude = models.DecimalField(max_digits=30, decimal_places=22, blank=True, null=True)
    altitude = models.DecimalField(max_digits=30, decimal_places=22, blank=True, null=True)
    accuracy = models.DecimalField(max_digits=30, decimal_places=22, blank=True, null=True)
    depth = models.DecimalField(max_digits=30, decimal_places=22, blank=True, null=True)
    common_name = models.CharField(max_length=255, blank=True, null=True)
    scientific_name = models.CharField(max_length=255, blank=True, null=True)
    capture_method = models.CharField(max_length=255, blank=True, null=True)
    weight = models.DecimalField(max_digits=30, decimal_places=22, blank=True, null=True)
    length = models.DecimalField(max_digits=30, decimal_places=22, blank=True, null=True)
    sex = models.CharField(choices=SEX, max_length=255, blank=True, null=True)
    habitat = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    action = models.TextField(blank=True, null=True)
    activity = models.TextField(blank=True, null=True)
    reporter = models.ForeignKey(User, default=1, on_delete=models.CASCADE)
    status = models.CharField(choices=STATUS, max_length=255, default='PENDING')
    rejected_reason = models.CharField(choices=REJECT_REASON, max_length=255, default=None, blank=True, null=True)
    submitter = models.CharField(max_length=255, blank=True, null=True)
    image_url = models.CharField(max_length=255, blank=True, null=True)


class ObservationName(models.Model):
    common_name = models.CharField(max_length=255, blank=True, null=True)
    scientific_name = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return f'{self.common_name} ({self.scientific_name})'


class ObservationReply(models.Model):
    observation = models.ForeignKey(Observation, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField(blank=True, null=True)
    is_verified = models.BooleanField(default=False)


class ObservationImage(models.Model):
    observation = models.ForeignKey(Observation, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='observations/%Y/%m/%d/')
    thumbnail = models.TextField(blank=True, null=True)

    def __str__(self):
        return f'{self.observation}'


class Topic(models.Model):
    slug = models.SlugField(max_length=255, unique=True)
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    image = models.ImageField(upload_to='forum/categories/', blank=True, null=True)

    def __str__(self):
        return self.title


class Thread(models.Model):
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=255, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    is_verified = models.BooleanField(default=False)

    def __str__(self):
        return self.title


class ThreadVote(models.Model):
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    vote = models.BooleanField(null=True)


class ThreadReply(models.Model):
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural = 'Thread Replies'


class Article(models.Model):
    en_title = models.CharField(max_length=255, blank=True, null=True)
    id_title = models.CharField(max_length=255, blank=True, null=True)
    author = models.CharField(max_length=255, blank=True, null=True)
    image = models.ImageField(upload_to='articles/', blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    en_content = RichTextField(blank=True, null=True)
    id_content = RichTextField(blank=True, null=True)

    def get_title(self):
        return self.id_title if self.id_title else self.en_title

    def __str__(self):
        return f'{self.get_title()} - ({self.author})'

class ArticleTag(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    en_tag = models.CharField(max_length=100, blank=True, null=True)
    id_tag = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return f'Tag {self.pk}'

class Page(models.Model):
    slug = models.SlugField()
    visibility = models.BooleanField(default=False)

    def __str__(self):
        return self.slug

class Navigation(models.Model):
    en_name = models.CharField(max_length=100, blank=True, null=True)
    id_name = models.CharField(max_length=100, blank=True, null=True)
    order = models.PositiveSmallIntegerField(blank=True, null=True)
    url = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.en_name

class SubNavigation(models.Model):
    navigation = models.ForeignKey(Navigation, on_delete=models.CASCADE)
    en_name = models.CharField(max_length=100, blank=True, null=True)
    id_name = models.CharField(max_length=100, blank=True, null=True)
    order = models.PositiveSmallIntegerField(blank=True, null=True)
    url = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.en_name

class Section(models.Model):
    SECTION_VARIANTS = [
        ('jumbotron', 'Jumbotron'),
        ('section','Section'),
        ('section_with_button','Section with Button'),
        ('section_with_two_image','Section with 2 Image'),
        ('section_with_three_image','Section with 3 Image'),
        ('section_gallery','Section Gallery'),
    ]

    level = models.ForeignKey(Page, on_delete=models.CASCADE)
    variant = models.CharField(choices=SECTION_VARIANTS, default='jumbotron', max_length=100)
    order = models.PositiveSmallIntegerField()
    en_title = models.CharField(max_length=255, blank=True, null=True)
    id_title = models.CharField(max_length=255, blank=True, null=True)
    en_content = RichTextField(blank=True, null=True)
    id_content = RichTextField(blank=True, null=True)
    
    SECTION_BACKGROUND_COLORS = [
        ('bg-blue', 'bg-blue'),
        ('bg-lightblue', 'bg-lightblue'),
        ('bg-white', 'bg-white'),
    ]
    
    BADGE_COLORS = [
        ('bg-yellow', 'bg-yellow'),
        ('bg-lightblue', 'bg-lightblue'),
    ]

    # button
    button_cta = models.CharField(max_length=255, blank=True, null=True)
    button_url = models.CharField(max_length=255, blank=True, null=True)

    # styles
    background_color = models.CharField(choices=SECTION_BACKGROUND_COLORS, default='bg-blue', max_length=100)
    badge_color = models.CharField(choices=BADGE_COLORS, default='bg-yellow', max_length=100)
    
    def __str__(self):
        return f'{self.pk}'

class SectionImage(models.Model):
    level = models.ForeignKey(Section, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='sections/', blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)