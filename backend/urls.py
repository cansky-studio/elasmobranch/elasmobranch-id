from django.urls import path
from backend import views

app_name = 'backend'

urlpatterns = [
    path('accounts/signup/', views.signup, name='signup'),
    path('accounts/signup/check/', views.signup_check, name='signup_check'),
    path('accounts/login/', views.login, name='login'),
    path('accounts/login/identify/', views.identify, name='identify'),
    path('accounts/logout/', views.logout, name='logout'),
    path('accounts/verify/', views.verify, name='verify'),
    path('accounts/verify/resend', views.verify_resend, name='verify_resend'),
    path('accounts/recovery/', views.recovery, name='recovery'),
    path('accounts/edit/', views.edit, name='edit'),

    path('interactions/follow/<int:pk>/', views.follow, name='follow'),
    path('interactions/unfollow/<int:pk>/', views.unfollow, name='unfollow'),

    path('interactions/up-vote/<int:pk>/', views.up_vote, name='up_vote'),
    path('interactions/down-vote/<int:pk>/', views.down_vote, name='down_vote'),

    path('thread/<slug>/', views.thread, name='thread'),
    path('thread/<int:pk>/edit/', views.thread_edit, name='thread_edit'),
    path('thread/<int:pk>/delete/', views.thread_delete, name='thread_delete'),
    path('thread/<int:pk>/reply/', views.thread_reply, name='thread_reply'),

    path('report/', views.report, name='report'),
    path('report/<int:pk>/edit', views.report_edit, name='report_edit'),

    path('observation/<int:pk>/reply', views.observation_reply, name='observation_reply'),
    path('observation/names/', views.observation_names, name='observation_names'),

    path('maps/', views.maps, name='maps'),
    path('search/', views.search, name='search'),
    path('language/<code>/', views.language, name='language'),

    path('mail-test/', views.mail_test, name='mail-test'),
]
