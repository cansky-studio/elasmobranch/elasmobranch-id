import secrets


def generic_multi_form(data):
    obs = {}

    for d in data:
        ds = d.split('-')
        field, key = ds[0], ds[1]
        value = data.get(d) if len(data.getlist(d)) < 2 else data.getlist(d)

        if not value:
            continue
        if key not in obs:
            obs[key] = {field: value}
        else:
            obs[key][field] = value

    return obs


def generic_single_form(data):
    obs = {}

    for d in data:
        value = data.get(d) if len(data.getlist(d)) < 2 else data.getlist(d)

        if not value:
            continue
        else:
            obs[d] = value

    return obs


def login_form(data):
    username = data.get("username", default=None)
    password = data.get("password", default=None)

    return {
        'username': username,
        'password': password
    }


def signup_form(data):
    username = data.get("username", default=None)
    first_name = data.get("first_name", default=None)
    last_name = data.get("last_name", default=None)
    job = data.get("job", default=None)
    phone = data.get("phone", default=None)
    birthday = data.get("birthday", default=None)
    email = data.get("email", default=None)
    language = data.get("language", default=None)
    password = data.get("password", default=None)
    token = secrets.token_hex(16)

    return {
        'username': username,
        'first_name': first_name,
        'last_name': last_name,
        'job': job,
        'phone': phone,
        'birthday': birthday,
        'email': email,
        'language': language,
        'password': password,
        'token': token
    }


def edit_form(data):
    first_name = data.get("first_name", default=None)
    last_name = data.get("last_name", default=None)
    job = data.get("job", default=None)
    phone = data.get("phone", default=None)
    birthday = data.get("birthday", default=None)
    language = data.get("language", default=None)
    description = data.get("description", default=None)

    return {
        'first_name': first_name,
        'last_name': last_name,
        'job': job,
        'phone': phone,
        'birthday': birthday,
        'language': language,
        'description': description,
    }
