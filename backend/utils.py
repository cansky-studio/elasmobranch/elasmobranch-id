from backend.models import ObservationImage


def save_multiple_images(observation, images):
    for image in images:
        save_image(observation, image)


def save_image(observation, image):
    check_file_extensions(image)
    obi = {'observation': observation, 'image': image}
    observation_image = ObservationImage(**obi)
    observation_image.save()


def check_file_extensions(file):
    filename = file.name
    allowed_extensions = ['jpg', 'jpeg', 'png']
    extension = filename.lower().split('.')[-1]
    if extension not in allowed_extensions:
        raise Exception('File extension not allowed')
