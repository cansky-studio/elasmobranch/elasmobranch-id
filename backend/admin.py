from django import forms
from django.contrib import admin, messages
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from import_export import resources
from import_export.admin import ImportExportModelAdmin

from nested_admin import NestedStackedInline, NestedModelAdmin  

from backend.mails import accepted_report_template, rejected_report_template
from backend.models import (
    User,
    Observation,
    ObservationName,
    ObservationImage,
    ObservationReply,
    Topic,
    Thread,
    ThreadReply,
    Article,
    ArticleTag,
    Page,
    Section,
    SectionImage,
    Navigation,
    SubNavigation,
)


class ObservationResource(resources.ModelResource):
    class Meta:
        model = Observation


class ObservationNameResource(resources.ModelResource):
    class Meta:
        model = ObservationName


class ObservationImageInline(admin.StackedInline):
    fields = ('preview_image', 'image', 'thumbnail',)
    readonly_fields = ('preview_image',)
    model = ObservationImage
    extra = 0

    def preview_image(self, obj):
        if obj.image:
            return mark_safe('<img src="{}" width="300" height="300" />'.format(obj.image.url))
        return mark_safe('<img src="https://via.placeholder.com/300" width="300" height="300" />')


class ObservationReplyInline(admin.StackedInline):
    model = ObservationReply
    extra = 0


class ObservationAdmin(ImportExportModelAdmin):
    resource_class = ObservationResource
    inlines = [ObservationImageInline, ObservationReplyInline]
    list_per_page = 25
    list_filter = ('status',)
    list_editable = (
        'common_name', 'scientific_name', 'category', 'status', 'rejected_reason', 'image_url',
        'location', 'province', 'latitude', 'longitude', 'reporter', 'submitter',
    )
    list_display = (
        'pk', 'common_name', 'scientific_name', 'category', 'status', 'rejected_reason',
        'get_image_status', 'image_url', 'location', 'province', 'latitude', 'longitude', 'reporter', 'submitter',
    )
    search_fields = ['common_name', 'scientific_name', ]

    def get_image_status(self, obj):
        return '✓' if obj.observationimage_set.all().count() > 0 else '✗'

    get_image_status.short_description = 'Image'

    def save_model(self, request, obj, form, change):
        if obj.pk is None:
            super().save_model(request, obj, form, change)
        else:
            old_obj = self.model.objects.get(id=obj.id)
            old_status = old_obj.status

            # already verified
            if old_status == 'VERIFIED':
                obj.status = 'VERIFIED'

            if obj.status == 'REJECTED' and not obj.rejected_reason:
                messages.error(request, f"Select Reject Reason for id {obj.pk}")
                return

            super().save_model(request, obj, form, change)

            user = User.objects.get(username=obj.reporter.username)
            new_status = obj.status

            if change and old_obj.status != 'VERIFIED':
                if new_status == 'VERIFIED':
                    accepted_report_template(user, obj)
                elif new_status == 'REJECTED':
                    rejected_report_template(user, obj)


class ObservationNameAdmin(ImportExportModelAdmin):
    resource_class = ObservationNameResource
    list_per_page = 25
    list_display = ('common_name', 'scientific_name',)


class ThreadReplyInline(admin.StackedInline):
    model = ThreadReply
    extra = 0


class ThreadAdmin(admin.ModelAdmin):
    inlines = [ThreadReplyInline]
    list_display = ('title', 'topic', 'user', 'is_verified',)


class UserAdmin(ImportExportModelAdmin):
    list_per_page = 25
    list_display = ('username', 'email', 'job', 'phone', 'birthday',)


class ArticleTagAdmin(admin.StackedInline):
    model = ArticleTag
    extra = 1

class ArticleAdmin(admin.ModelAdmin):
    inlines = [ArticleTagAdmin]


class SectionImageAdmin(NestedStackedInline):
    model = SectionImage
    extra = 0

class SectionAdmin(NestedStackedInline):
    model = Section
    extra = 0
    ordering = ('order',)
    inlines = [SectionImageAdmin]

    fieldsets = (
        ('Content', {
           'fields': ('variant', 'order', 'en_title', 'en_content', 'id_title', 'id_content')
        }),
        ('Style', {
            'fields': ('background_color', 'badge_color'),
        }),
        ('Button', {
            'fields': ('button_cta', 'button_url'),
        }),
    )

class PageAdmin(NestedModelAdmin):
    model = Page
    inlines = [SectionAdmin]
    list_display = ('slug','show_url','visibility',)

    def get_queryset(self, request):
        self.full_path =  "{0}://{1}".format(request.scheme, request.get_host())
        return super().get_queryset(request)

    @admin.display(description=_("Preview"))
    def show_url(self, obj):
        return format_html(f"<a href='{self.full_path}/s/{obj.slug}?preview=True'>Link</a>")

class SubNavigationAdmin(admin.StackedInline):
    model = SubNavigation
    extra = 0

class NavigationAdmin(admin.ModelAdmin):
    inlines = [SubNavigationAdmin]

admin.site.register(User, UserAdmin)
admin.site.register(Observation, ObservationAdmin)
admin.site.register(ObservationName, ObservationNameAdmin)
admin.site.register(Thread, ThreadAdmin)
admin.site.register(Topic)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(Navigation, NavigationAdmin)

admin.site.site_header = "Elasmobranch Project Indonesia"
admin.site.site_title = "Elasmobranch Project Indonesia"
admin.site.index_title = "Welcome to Elasmobranch Project Indonesia Admin"
