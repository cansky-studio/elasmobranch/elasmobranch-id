import json
import re

from django.contrib import messages
from django.contrib.auth import (
    login as dj_login,
    logout as dj_logout,
    authenticate as dj_authenticate,
)
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.core.mail import send_mail
from django.http import JsonResponse
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt

from backend.mails import verify_user_template, forgot_password_template
from frontend.utils import set_session_lang

from backend import forms, utils
from backend.models import (
    User, UserConnection,
    Observation, ObservationName, ObservationImage, ObservationReply,
    Topic, Thread, ThreadReply, ThreadVote
)


# Accounts 
def signup(request):
    if request.method == 'POST':
        try:
            email = request.POST.get('email')
            users = User.objects.filter(email=email)
            if users.exists():
                raise Exception('Duplicate Email')

            username = request.POST.get('username')

            pattern = "^[A-Za-z0-9_-]*$"
            match = bool(re.match(pattern, username))

            if not username or len(username) < 3 or not match:
                raise Exception('Invalid Username')

            data = forms.signup_form(request.POST)
            password = data.pop("password")

            new_user = User.objects.create(**data)
            new_user.set_password(password)
            new_user.save()

            if new_user:
                verify_user_template(new_user)
                dj_login(request, new_user)

                return redirect('frontend:home')
        except Exception as e:
            messages.error(request, e)

    return redirect('frontend:signup')


def signup_check(request):
    username = request.GET.get('username')
    flag = False

    if username:
        pattern = "^[A-Za-z0-9_-]*$"
        match = bool(re.match(pattern, username))

        if len(username) > 3 and match:
            exists = User.objects.filter(username=username).exists()
            flag = not exists

    data = {'is_available': flag}
    return JsonResponse(data)


def edit(request):
    try:
        username = request.user.username
        data = forms.edit_form(request.POST)
        image = request.FILES.get('image', None)

        # update multiple fields
        User.objects.filter(username=username).update(**data)

        if image:
            utils.check_file_extensions(image)
            user = User.objects.get(username=username)
            user.image = image
            user.save()

    except Exception as e:
        messages.error(request, e)

    return redirect('frontend:contributor_profile')


def recovery(request):
    try:
        token = request.GET.get('token', None)
        users = User.objects.filter(token=token)

        if not users.exists():
            raise Exception('Invalid User Token')

        password = request.POST.get('password')
        confirm_password = request.POST.get('confirm_password')

        if password != confirm_password:
            raise Exception('Invalid Request')

        user = users.first()
        user.set_password(password)
        user.save()

        if not request.user.is_authenticated:
            dj_login(request, user)

        toast_content = 'Success Update Password'
        messages.success(request, toast_content)

        return redirect('frontend:home')
    except Exception as e:
        messages.error(request, e)

    return redirect('frontend:recovery')


def verify(request):
    try:
        token = request.GET.get('token', None)
        users = User.objects.filter(token=token)
        if not users.exists():
            raise Exception('Invalid User Token')

        user = users.first()
        user.is_verified = True
        user.save()

        dj_login(request, user)

        toast_content = 'User Verified'
        messages.success(request, toast_content)

        return redirect('frontend:home')
    except Exception as e:
        messages.error(request, e)

    return redirect('frontend:verify')


def verify_resend(request):
    try:
        user = User.objects.get(username=request.user.username)
        verify_user_template(user)

        toast_content = 'Re-send Email Verification'
        messages.success(request, toast_content)

        return redirect('frontend:home')
    except Exception as e:
        messages.error(request, e)

    return redirect('frontend:verify')


def login(request):
    if request.method == 'POST':
        try:
            data = forms.login_form(request.POST)
            existing_user = dj_authenticate(**data)

            if not existing_user:
                raise Exception('Invalid username or password')

            dj_login(request, existing_user)
            return redirect('frontend:contributor_profile')



        except Exception as e:
            messages.error(request, e)

    return redirect('frontend:login')


def identify(request):
    if request.method == "POST":
        try:
            email = request.POST.get("email", None)
            user = User.objects.filter(email=email)

            if user.exists():
                user = user.first()
                forgot_password_template(user)

                toast_content = 'Check your email'
                messages.info(request, toast_content)

                return redirect('frontend:home')
            else:
                raise Exception("Email doesn't exists")
        except Exception as e:
            messages.error(request, e)

    return redirect('frontend:identify')


def logout(request):
    try:
        dj_logout(request)
        return redirect('frontend:login')
    except Exception as e:
        messages.error(request, e)

    return redirect('frontend:home')


def follow(request, pk):
    try:
        user = User.objects.get(pk=pk)
        data = {'follower': request.user, 'following': user}
        connection = UserConnection(**data)
        connection.save()
    except Exception as e:
        return JsonResponse({'message': e})

    return JsonResponse({'status': 'OK'})


def unfollow(request, pk):
    try:
        user = User.objects.get(pk=pk)
        data = {'follower': request.user, 'following': user}
        connection = UserConnection.objects.filter(**data)
        connection.delete()
    except Exception as e:
        return JsonResponse({'message': e})

    return JsonResponse({'status': 'OK'})


@login_required
def up_vote(request, pk):
    try:
        user = User.objects.get(username=request.user.username)

        if True:
            thread = Thread.objects.get(pk=pk)
            data = {'thread': thread, 'user': user, 'vote': True}

            thread_votes = ThreadVote.objects.filter(thread=thread, user=user)
            if thread_votes.exists():
                _ = thread_votes.update(**data)
            else:
                thread_vote = ThreadVote(**data)
                thread_vote.save()

    except Exception as e:
        messages.error(request, e)

    return redirect("frontend:forum_thread", pk=pk)


@login_required
def down_vote(request, pk):
    try:
        user = User.objects.get(username=request.user.username)

        if True:
            thread = Thread.objects.get(pk=pk)
            data = {'thread': thread, 'user': user, 'vote': False}

            thread_votes = ThreadVote.objects.filter(thread=thread, user=user)
            if thread_votes.exists():
                _ = thread_votes.update(**data)
            else:
                thread_vote = ThreadVote(**data)
                thread_vote.save()

    except Exception as e:
        messages.error(request, e)

    return redirect("frontend:forum_thread", pk=pk)


def search(request):
    if request.method == "POST":
        query = request.POST.get('query', None)
        if query:
            return redirect(f'/search?query={query}')

    return redirect('frontend:maps')


@csrf_exempt
@login_required
def report(request):
    try:
        data = forms.generic_multi_form(request.POST)
        data_images = forms.generic_multi_form(request.FILES)

        for key in data.keys():
            item = data[key]

            item['type'] = 'sighting' if 'S' in key else 'landing'
            item['reporter'] = request.user

            if 'habitat' in item:
                if isinstance(item['habitat'], list):
                    item['habitat'] = ','.join(item['habitat'])

            observation = Observation(**item)
            observation.save()

            item_images = data_images[key]['upload']
            is_multi = isinstance(item_images, list)

            if is_multi:
                utils.save_multiple_images(observation, item_images)
            else:
                utils.save_image(observation, item_images)
    except Exception as e:
        messages.error(request, e)

    return redirect('frontend:contributor_profile')


@csrf_exempt
@login_required
def report_edit(request, pk):
    try:
        observation = Observation.objects.get(pk=pk)

        data = forms.generic_single_form(request.POST)
        data_images = forms.generic_single_form(request.FILES)

        if 'habitat' in data:
            data['habitat'] = ','.join(data['habitat'])

        # change status to pending
        data['status'] = 'PENDING'

        # update data
        _ = Observation.objects.filter(pk=pk).update(**data)

        if data_images:
            # remove old photos
            _ = ObservationImage.objects.filter(observation=observation).delete()

            utils.save_image(observation, data_images['upload'])
    except Exception as e:
        messages.error(request, e)

    return redirect('frontend:contributor_profile')


def observation_names(request):
    obs_names = ObservationName.objects.all()
    common_names = obs_names.order_by('common_name').values_list('common_name', flat=True)
    scientific_names = obs_names.order_by('scientific_name').values_list('scientific_name', flat=True)

    response = {
        'common_names': list(common_names),
        'scientific_names': list(scientific_names),
    }

    return JsonResponse(response, safe=False)


def maps(request):
    filters = request.GET

    categories = filters.getlist('category', [])
    types = filters.getlist('type', [])
    years = filters.getlist('year', [])

    observations = Observation.objects.filter(status='VERIFIED')

    if categories:
        observations = observations.filter(category__in=categories)

    if types:
        observations = observations.filter(type__in=types)

    if years:
        observations = observations.filter(datetime__year__in=years)

    observations = json.loads(serializers.serialize('json', observations))

    for observation in observations:
        ob_images = ObservationImage.objects.filter(observation__pk=observation['pk'])
        if ob_images.exists():
            ob_image = ob_images.first()
            observation['fields']['image'] = ob_image.image.url

    return JsonResponse(observations, safe=False)


def language(request, code):
    set_session_lang(request, code)
    return redirect("frontend:home")


def observation_reply(request, pk):
    if request.method == "POST":
        content = request.POST.get('content', None)
        observation = Observation.objects.get(pk=pk)

        data = {'content': content, 'observation': observation, 'user': request.user}

        new_observation_reply = ObservationReply(**data)
        new_observation_reply.save()

    return redirect("frontend:contributor_observation", pk=pk)


def thread(request, slug):
    if request.method == "POST":
        try:
            title = request.POST.get('title', None)
            content = request.POST.get('content', None)

            topic = Topic.objects.get(slug=slug)

            data = {'title': title, 'content': content, 'topic': topic, 'user': request.user}

            new_thread = Thread(**data)
            new_thread.save()

            return redirect(f'/forum/threads/{slug}/')
        except Exception as e:
            messages.error(request, e)


def thread_edit(request, pk):
    if request.method == "POST":
        try:
            title = request.POST.get('title', None)
            content = request.POST.get('content', None)

            data = {'title': title, 'content': content}
            _ = Thread.objects.filter(pk=pk).update(**data)

        except Exception as e:
            messages.error(request, e)

    return redirect(f'/forum/thread/{pk}/')


def thread_delete(request, pk):
    try:
        thread = Thread.objects.get(pk=pk)
        slug = thread.topic.slug

        thread.delete()

        return redirect(f'/forum/threads/{slug}/')

    except Exception as e:
        messages.error(request, e)

    return redirect(f'/forum/thread/{pk}/')


def thread_reply(request, pk):
    if request.method == "POST":
        try:
            content = request.POST.get('content', None)
            thread = Thread.objects.get(pk=pk)

            data = {'content': content, 'thread': thread, 'user': request.user}

            new_thread_reply = ThreadReply(**data)
            new_thread_reply.save()

            return redirect('frontend:forum_thread', pk=pk)
        except Exception as e:
            messages.error(request, e)


def mail_test(request):
    test = {
        'subject': 'connect mail with elasmobranch',
        'message': 'open connection',
        'from_email': 'no-reply@elasmobranch.id',
        'html_message': 'Try to open connection between us',
        'recipient_list': ['canskystudio@gmail.com'],
        'fail_silently': True
    }

    send_mail(**test)
    return None
